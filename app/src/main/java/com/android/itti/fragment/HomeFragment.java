package com.android.itti.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.android.itti.R;

/**
 * Created by Prabhav Pulunghang on 9/5/2016.
 */
public class HomeFragment extends Fragment {
    View view;
    private WebView mWebView;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_layout, container, false);

        activity = this.getActivity();

        mWebView = (WebView) view.findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                activity.setProgress(progress * 1000);
            }
        });

        mWebView.loadUrl("http://itti.com.np");

        this.mWebView.getSettings().setUserAgentString(
                this.mWebView.getSettings().getUserAgentString()
                        + " "
                        + getString(R.string.user_agent_suffix)
        );

        final SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Insert your code here
                mWebView.reload(); // refreshes the WebView
                swipeLayout.setRefreshing(false);
            }
        });

        return view;
    }
}
